#!/bin/sh
set -e

# AWS Envs
export AWS_HOST_IP=$(curl -sf -m 3 http://169.254.169.254/latest/meta-data/local-ipv4)

# Host IP DNS
[ ! -z "$AWS_HOST_IP" ] && final_host_ip="$AWS_HOST_IP"
[ ! -z "$HOST_IP" ] && final_host_ip="$HOST_IP"

[ ! -z "$final_host_ip" ] && sudo sh -c "echo $final_host_ip host-ip >> /etc/hosts"

export HOST_IP="$final_host_ip"
STORAGE_PATH=/var/www/html/storage_nfs
chmod -R 777 $STORAGE_PATH /var/www/html/bootstrap/cache

if [[ -f .env ]]; then
    touch .environment-system && echo "system env" >> .environment-system

    #laravel steps
#    if [[ ! -f "/var/www/html/storage/oauth-public.key" && ! -f "/var/www/html/storage/oauth-private.key" ]];
#    then
#        php artisan key:generate
#    fi

    php artisan migrate
    php artisan passport:install
fi

# Run Consul Register Control
#curl -X "PUT" "http://host-ip:8500/v1/agent/service/register" \
# -H 'Content-Type: application/json' \
# -d $'{
#    "ID": "api-service",
#    "Address": "$HOST_IP",
#    "Name": "api-service",
#    "Check": {
#      "HTTP":"http://$HOST_IP:8003/_health",
#      "Interval": "10s",
#      "DeregisterCriticalServiceAfter": "1m"
#    },
#    "Port": 8003,
#    "tags": ["api-service","traefik.frontend.entryPoints=http","traefik.frontend.passHostHeader=true","traefik.enable=true","traefik.frontend.rule=PathPrefixStrip:/"]
#  }'
#

#
#curl \
#    --request PUT \
#    http://127.0.0.1:8500/v1/agent/service/deregister/my-service-id
#
# python /logger-service/consul_script.py
rm -rf ${STORAGE_PATH}/framework/cache/*
php artisan config:cache
php artisan cache:clear

## Nginx permissions for prevent access error for swaggerr UI
chown -R www-data:www-data /var/lib/nginx
chown -R www-data:www-data /var/tmp/nginx
ls -lA /var/lib
ls -lA /var/tmp
echo "###Teste de deploy Rafael Barbosa"

exec "$@"


#curl -X "PUT" "http://host-ip:8500/v1/agent/service/register" \
# -H 'Content-Type: application/json' \
# -d $'{
#    "ID": "api-service",
#    "Address": "10.139.1.176",
#    "Name": "api-service",
#    "Check": {
#      "HTTP":"http://10.139.1.176:8003/_health",
#      "Interval": "10s",
#      "DeregisterCriticalServiceAfter": "1m"
#    },
#    "Port": 8003,
#    "tags": ["api-service","traefik.frontend.entryPoints=http","traefik.frontend.passHostHeader=true","traefik.port=80", "traefik.port=443", "traefik.enable=true","traefik.frontend.rule=PathPrefixStrip:/"]
#  }'
##
##
#curl -X "PUT" "http://host-ip:8500/v1/agent/service/register" \
# -H 'Content-Type: application/json' \
# -d $'{
#    "ID": "api-service",
#    "Address": "10.139.2.244",
#    "Name": "api-service",
#    "Check": {
#      "HTTP":"http://10.139.2.244:8003/_health",
#      "Interval": "10s",
#      "DeregisterCriticalServiceAfter": "1m"
#    },
#    "Port": 8003,
#    "tags": [
#        "api-service",
#        "traefik.frontend.rule=Host:dev-o4r88wze-cluster.novacytes.com.br;PathPrefixStrip:/",
#        "traefik.frontend.entryPoints=http,https",
#        "traefik.frontend.passHostHeader=true",
#        "traefik.enable=true"
#        ]
#  }'
#
#
#
#
#
#
#
#
#curl -X "PUT" "http://host-ip:8500/v1/agent/service/register" \
# -H 'Content-Type: application/json' \
# -d $'{
#    "ID": "api-service",
#    "Address": "10.139.1.176",
#    "Name": "api-service",
#    "Check": {
#      "HTTP":"http://10.139.1.176:8003/_health",
#      "Interval": "10s",
#      "DeregisterCriticalServiceAfter": "1m"
#    },
#    "Port": 8003,
#    "tags": [
#        "api-service",
#        "traefik.frontend.rule=Host:http://dev-ddmzwjhp-dev-1127532589.us-east-1.elb.amazonaws.com;PathPrefixStrip:/api-service",
#        "traefik.frontend.entryPoints=http",
#        "traefik.frontend.passHostHeader=true",
#        "traefik.enable=true",
#        "traefik.frontend.redirect.regex=^http://dev-ddmzwjhp-dev-1127532589.us-east-1.elb.amazonaws.com/(.*)",
#        "traefik.frontend.redirect.replacement=http://dev-ddmzwjhp-dev-1127532589.us-east-1.elb.amazonaws.com/api-service/$1",
#        "traefik.frontend.rule=PathPrefix:/api-service;ReplacePathRegex: ^/api-service/(.*) /$$1"
#        ]
#  }'




#curl -X "PUT" "http://host-ip:8500/v1/agent/service/register" \
# -H 'Content-Type: application/json' \
# -d $'{
#    "ID": "api-service",
#    "Address": "10.139.2.244",
#    "Name": "api-service",
#    "Check": {
#      "HTTP":"http://10.139.2.244:8003/_health",
#      "Interval": "10s",
#      "DeregisterCriticalServiceAfter": "1m"
#    },
#    "Port": 8003,
#    "tags": ["api-service","traefik.frontend.entryPoints=http,https","traefik.frontend.passHostHeader=true","traefik.port=80", "traefik.port=443", "traefik.enable=true","traefik.frontend.rule=PathPrefixStrip:/"]
#  }'