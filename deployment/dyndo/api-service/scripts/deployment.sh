#!/bin/bash
# Description   : Script for "Build and Upload to AWS ECR" task.
# Author        : Rafael Milani Barbosa
# Updated       : 2020/03/02
# Date          : 2020/03/02

# requirements
# python 3.7.6
# pip 20.0.2
# awscli 1.18.5

helpFunction()
{
   echo ""
   echo "-------------------------------------------------------"
   echo "ERROR: Bad usage."
   echo "-------------------------------------------------------"
   echo "Usage: $0 --env=ENV --nodes=NODE --unitTest=TRUE"
   echo -e "\t-env \t\tEnvironment type. Ex: development/testing/staging/production"
   echo -e "\t-nodes \t\tNumber of cluster nodes. Default: 1"
   echo -e "\t-unitTest \tEnable Unit Tests. Defaault: false"
   echo "-------------------------------------------------------"
   exit 1 # Exit script after printing help
}

printStepInfo()
{
    echo "-------------------------------------------------------"
    echo $1
    echo "-------------------------------------------------------"
}

# extract options and their arguments into variables.
for ARGUMENT in "$@"
do
    KEY=$(echo $ARGUMENT | cut -f1 -d=)
    VALUE=$(echo $ARGUMENT | cut -f2 -d=)

    case "$KEY" in
            -e|--env)            _ENV=${VALUE} ;;
            -n|--nodes)          _NODES=${VALUE} ;;
            -u|--unitTest)       _UNITTEST=${VALUE} ;;
            *)                   helpFunction ;;
    esac
done

# Print helpFunction in case parameters are empty
if [ -z "$_ENV" ] || [ -z "$_NODES" ] || [ -z "$_UNITTEST" ]
then
   echo "Some or all of the parameters are empty";
   helpFunction
fi

printStepInfo "Validate environment"
case "$_ENV" in
  development)
    _SHORT_ENV=dev ;;
  staging)
    _SHORT_ENV=staging ;;
  testing)
    _SHORT_ENV=test ;;
  production)
    _SHORT_ENV=prod ;;
esac

printStepInfo "Check installed dependencies"

AWS_CURRENT_VERSION=1.18.5
AWS_VERSION=$( aws --version  | cut -d " " -f 1 | cut -d "/" -f 2 )

if [[ ! $AWS_VERSION == $AWS_CURRENT_VERSION ]]
then
  echo "You can't usage the required aws current version . [$AWS_CURRENT_VERSION] "
  echo "AWS Version: " $AWS_VERSION
  echo "Please run: python -m pip install awscli==1.18.5"
  exit 1
fi

printStepInfo "Application Deployment Script Started"

printStepInfo "#### Git submodules"
git submodule update --init --recursive

printStepInfo "#### Export system environments to laravel env file"
if [[ ! -f "cloud-ops/deployment/dyndo/api-service/app/.env.$_ENV" ]]
then
    echo "Evironment File [cloud-ops/deployment/dyndo/api-service/app/.env.$_ENV] not found."
    exit 1
fi
cp -R cloud-ops/deployment/dyndo/api-service/app/.env.$_ENV .env
export $(egrep -v '^#' .env | xargs)

printStepInfo "#### Aws login"
eval $(aws ecr get-login --region ${AWS_DEFAULT_REGION} --no-include-email)

printStepInfo "#### Get Deployment commit hash"
if [ -z "${BITBUCKET_COMMIT}" ]
then
      export DEPLOYMENT_COMMIT_HASH=$(git rev-parse HEAD)
else
      export DEPLOYMENT_COMMIT_HASH=${BITBUCKET_COMMIT}
fi

printStepInfo "#### Get Deployment build number"
if [ -z "${BITBUCKET_BUILD_NUMBER}" ]
then
      export DEPLOYMENT_BUILD_NUMBER=local
else
      export DEPLOYMENT_BUILD_NUMBER=${BITBUCKET_BUILD_NUMBER}
fi

printStepInfo "#### Get Service name"
export SERVICE_NAME=${_SHORT_ENV}-${APP_SERVICE_NAME}

printStepInfo "#### Get Registry URL"
export REGISTRY_URL=$(aws ecr describe-repositories | jq -r '.repositories[] | select( .repositoryArn | contains("'${SERVICE_NAME}'")) | (.repositoryUri)')

printStepInfo "#### Get Registry ID"
export REGISTRY_ID=$(aws ecr describe-repositories | jq -r '.repositories[] | select( .repositoryArn | contains("'${SERVICE_NAME}'")) | (.registryId)')

printStepInfo "#### Get Registry Name"
export REGISTRY_NAME=$(aws ecr describe-repositories | jq -r '.repositories[] | select( .repositoryArn | contains("'${SERVICE_NAME}'")) | (.repositoryName)')

printStepInfo "#### Clear images by filtering the last 2 images"
countImages=0
for row in $(aws ecr describe-images --repository-name $REGISTRY_NAME --registry-id $REGISTRY_ID | jq -r '.imageDetails | map({imageDigest: .imageDigest, date: .imagePushedAt}) | sort_by(.date) | reverse' | jq '.[].imageDigest');
do
  if [ $countImages -gt 2 ]
  then
      echo "Delete Image: " $row
      aws ecr batch-delete-image --repository-name $REGISTRY_NAME --registry-id $REGISTRY_ID --image-ids imageDigest=$row
  fi
  ((countImages=countImages+1))
done

printStepInfo "#### Build Docker Image"
export BUILD_ID=${DEPLOYMENT_COMMIT_HASH}_${DEPLOYMENT_BUILD_NUMBER}
echo "" >> .env
echo "BUILD_ID=$BUILD_ID" >> .env

docker build --build-arg APP_STORAGE=${APP_STORAGE} -f cloud-ops/deployment/dyndo/api-service/docker/Dockerfile -t ${REGISTRY_URL}:$BUILD_ID .

printStepInfo "#### Push Docker Image"
docker push ${REGISTRY_URL}:$BUILD_ID
docker tag ${REGISTRY_URL}:$BUILD_ID ${REGISTRY_URL}:latest
docker push ${REGISTRY_URL}:latest

printStepInfo "#### Get [$_ENV] cluster name"
export CLUSTER_ECS_NAME=$(aws ecs list-clusters | jq -r '.clusterArns[]' | grep $_SHORT_ENV | sed -n 's/.*\('$_SHORT_ENV'\)/\1/p')
echo $CLUSTER_ECS_NAME

printStepInfo "#### Get role arn"
export ROLE_ARN=$(aws iam get-role --role-name dev_ecs_instance_role | jq -r '.Role.Arn')
echo $ROLE_ARN

printStepInfo "#### Clear task definitions by filtering the last 2 task definitions"
countDefinitions=0
for row in $(aws ecs list-task-definitions --family-prefix $SERVICE_NAME --sort DESC | jq '.taskDefinitionArns[]');
do
    if [ $countDefinitions -gt 2 ]
    then
        echo "Delete Task Definition:" $row
        REVISION_NUMBER=$( echo ${row#*task-definition/} | sed 's/\([^"]\)"/\1/g' )
        aws ecs deregister-task-definition --task-definition $REVISION_NUMBER
    fi
    ((countDefinitions=countDefinitions+1))
done

printStepInfo "#### Create and register task definition"
aws ecs register-task-definition --execution-role-arn $ROLE_ARN --cli-input-json file://ecs-task-definition.${_ENV}.json

printStepInfo "#### Get latest registered task definition"
export TASK_DEFINITION=$(aws ecs list-task-definitions --sort DESC | jq -r '.taskDefinitionArns[]' | grep $SERVICE_NAME | head -n1 | sed -n 's/.*\('$SERVICE_NAME'\)/\1/p')

printStepInfo "#### Stop all preview tasks by user"
aws ecs list-tasks --cluster $CLUSTER_ECS_NAME | jq -r ".taskArns[]" | awk '{print "aws ecs stop-task --cluster $CLUSTER_ECS_NAME --task \""$0"\""}' | sh

printStepInfo "#### Create ecs service"
SERVICE_STATUS=$(aws ecs describe-services --services $SERVICE_NAME --cluster $CLUSTER_ECS_NAME | jq -r '.services[] | (.status)')

if [[ -z $SERVICE_STATUS || $SERVICE_STATUS == "INACTIVE" ]]
then
  echo "#### Creating service: $SERVICE_NAME "
  aws ecs create-service --cluster $CLUSTER_ECS_NAME --service-name $SERVICE_NAME --task-definition $TASK_DEFINITION --desired-count $_NODES --launch-type "EC2"
else
  echo "#### Updating service: $SERVICE_NAME "
  aws ecs update-service --cluster $CLUSTER_ECS_NAME --service $SERVICE_NAME --task-definition $TASK_DEFINITION --desired-count $_NODES --force-new-deployment
fi

printStepInfo "#### Succesful Deployed."