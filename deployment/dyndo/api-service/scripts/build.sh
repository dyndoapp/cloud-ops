#!/bin/bash
# Description   : Script for "Build Application" task.
# Author        : Rafael Milani Barbosa
# Updated       : 2020/03/10
# Date          : 2020/03/10

echo  "--------------------------------------------------"
echo  "Application Build Script Started"
echo  "--------------------------------------------------"

echo  "#### Install dependencies"
echo  "--------------------------------------------------"
composer install