#!/bin/bash

# Begin Standard 'imports'
set -e
set -o pipefail

gray="\\e[37m"
blue="\\e[36m"
red="\\e[31m"
green="\\e[32m"
reset="\\e[0m"

info() {
  echo -e "${blue}-------------------------------------------------------"
  echo -e "${blue}INFO: $*${reset}";
  echo -e "${blue}-------------------------------------------------------"
}
error() {
  echo -e "${red}-------------------------------------------------------"
  echo -e "${red}ERROR: $*${reset}";
  echo -e "${red}-------------------------------------------------------"
}
debug() {
  if [[ "${DEBUG}" == "true" ]]; then
    echo -e "${gray}-------------------------------------------------------"
    echo -e "${gray}DEBUG: $*${reset}";
    echo -e "${gray}-------------------------------------------------------"
  fi
}

success() {
  echo -e "${green}-------------------------------------------------------"
  echo -e "${green}✔ $*${reset}";
  echo -e "${green}-------------------------------------------------------"
}
fail() {
  echo -e "${red}-------------------------------------------------------"
  echo -e "${red}✖ $*${reset}"; exit 1;
  echo -e "${red}-------------------------------------------------------"
}

## Enable debug mode.
enable_debug() {
  aws_debug_args=""
  if [[ "${DEBUG}" == "true" ]]; then
    info "Enabling debug mode."
    set -x
    aws_debug_args="--debug"
  fi
}

# Execute a command, saving its output and exit status code, and echoing its output upon completion.
# Globals set:
#   status: Exit status of the command that was executed.
#   output: Output generated from the command.
#
run() {
  echo "$@"
  set +e
  output=$("$@" 2>&1)
  status=$?
  set -e
  echo "${output}"
}

# End standard 'imports'
