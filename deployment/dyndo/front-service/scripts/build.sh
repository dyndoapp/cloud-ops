#!/bin/bash
# Description   : Script for "Build Application" task.
# Author        : Rafael Milani Barbosa
# Updated       : 2020/03/29
# Date          : 2020/03/29

source "$(dirname "$0")/common.sh"

info "Application Build Script Started"

mv cloud-ops/deployment/dyndo/front-service/app/.env.development .env
cat .env
eval $(cat .env | sed 's/^/export /')
yarn install
quasar build
echo "REPO FULL NAME:${BITBUCKET_REPO_FULL_NAME}" >> dist/spa/rev.txt
echo "BUILD NUMBER:${BITBUCKET_BUILD_NUMBER}" >> dist/spa/rev.txt
echo "HASH COMMIT:${BITBUCKET_COMMIT}" >> dist/spa/rev.txt
echo "BRANCH:${BITBUCKET_BRANCH}" >> dist/spa/rev.txt
echo "TAG:${BITBUCKET_TAG}" >> dist/spa/rev.txt

success "Build Successful."