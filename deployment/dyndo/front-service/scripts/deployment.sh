#!/bin/bash
# Description   : Script for "Deploy to AWS S3" task.
# Author        : Rafael Milani Barbosa
# Updated       : 2020/03/29
# Date          : 2020/03/29
#
# Required globals:
#   AWS_ACCESS_KEY_ID
#   AWS_SECRET_ACCESS_KEY
#   AWS_DEFAULT_REGION
#   S3_BUCKET
#   LOCAL_PATH
#
# Optional globals:
#   CONTENT_ENCODING
#   ACL
#   STORAGE_CLASS
#   CACHE_CONTROL
#   EXPIRES
#   DELETE_FLAG
#   EXTRA_ARGS
#   DEBUG

source "$(dirname "$0")/common.sh"

# Set env
info "Set env and mandatory dependencies."
eval $(cat .env | sed 's/^/export /')
env

# mandatory parameters
AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID:?'AWS_ACCESS_KEY_ID variable missing.'}
AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY:?'AWS_SECRET_ACCESS_KEY variable missing.'}
AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION:?'AWS_DEFAULT_REGION variable missing.'}
S3_BUCKET=${S3_BUCKET:?'S3_BUCKET variable missing.'}
LOCAL_PATH=${LOCAL_PATH:?'LOCAL_PATH variable missing.'}

if [[ ! -d "${LOCAL_PATH}" ]]; then
  fail "LOCAL_PATH must be a directory."
fi

info "Starting deployment to S3..."
run aws s3 sync ${LOCAL_PATH} s3://${S3_BUCKET}/ --cache-control ${CACHE_CONTROL} --acl ${ACL} ${aws_debug_args}
if [[ "${status}" -eq 0 ]]; then
  success "Deployment successful."
else
  fail "Deployment failed."
fi